-- Data preparation

create or replace table seqs (
    seq int,
    id int,
    ts int,
    name varchar,
    attr_0 varchar default null,
    attr_1 varchar default null,
    attr_2 varchar default null,
    attr_3 varchar default null,
    attr_4 varchar default null,
    attr_5 varchar default null,
    attr_6 varchar default null,
    attr_7 varchar default null,
    attr_8 varchar default null,
    attr_9 varchar default null,
    PRIMARY KEY (seq, id)
);

PUT file:///mnt/c/Users/pmm-workstation/workspace/seq/gen-100000000-0.pandas @my_test_stag                              e;

copy into test1 from (select $1 from @my_test_stage/gen-100000000-0.pandas) file_format = (type=PARQUET compression=SNAPPY);

insert into seqs (seq, id, ts, name,
                attr_0, attr_1, attr_2, attr_3, attr_4,
                attr_5, attr_6, attr_7, attr_8, attr_9)
(
  select
  $1:seq::int,
  $1:id::int,
  $1:ts::int,
  $1:name::varchar,
  $1:attr_0::varchar,
  $1:attr_1::varchar,
  $1:attr_2::varchar,
  $1:attr_3::varchar,
  $1:attr_4::varchar,
  $1:attr_5::varchar,
  $1:attr_6::varchar,
  $1:attr_7::varchar,
  $1:attr_8::varchar,
  $1:attr_9::varchar
  from test1);

-- from (select $1 from @my_test_stage/gen-100000000-0.pandas file_format = (type=PARQUET compression=SNAPPY));

-- some basic queries

select count(*) from seqs;
/*
+-----------+
|  COUNT(*) |
|-----------|
| 100000000 |
+-----------+
1 Row(s) produced. Time Elapsed: 0.102s
*/

select * from seqs limit 3;

/*
+----------+----------+------------+------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+
|      SEQ |       ID |         TS | NAME | ATTR_0 | ATTR_1 | ATTR_2 | ATTR_3 | ATTR_4 | ATTR_5 | ATTR_6 | ATTR_7 | ATTR_8 | ATTR_9 |
|----------+----------+------------+------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------|
| 10475999 | 25280512 | 1652831287 | C    |        |        |        |        | bar    |        |        | bar    |        | baz    |
| 10022102 | 25280513 | 1652878072 | B    | baz    |        | bar    |        |        |        |        |        |        | bar    |
| 10966312 | 25280514 | 1652820605 | D    | baz    |        |        | baz    |        | foo    |        | baz    |        |        |
+----------+----------+------------+------+--------+--------+--------+--------+--------+--------+--------+--------+--------+--------+
3 Row(s) produced. Time Elapsed: 0.654s
*/

select count(*) from seqs join
    (select distinct seq as selected_seq from seqs where attr_0='foo') on seq=selected_seq;
/*
+----------+
| COUNT(*) |
|----------|
| 99834521 |
+----------+
1 Row(s) produced. Time Elapsed: 2.254s
 */

-- Selecting sequences where any item as attr_0=foo
-- (and then filtering it to just one)
SELECT
    a.seq,
    ARRAY_AGG(a.name)
WITHIN GROUP (ORDER BY a.id ASC)
FROM seqs a
JOIN
    (SELECT DISTINCT seq AS selected_seq FROM seqs WHERE attr_0='foo')
ON seq=selected_seq
WHERE
    a.seq=10032771
GROUP BY
    a.seq;

/*
+----------+----------------------------------+
|      SEQ | ARRAY_AGG(A.NAME)                |
|          | WITHIN GROUP (ORDER BY A.ID ASC) |
|----------+----------------------------------|
| 10032771 | [                                |
|          |   "A",                           |
|          |   "D",                           |
...
|          |   "B",                           |
|          |   "A"                            |
|          | ]                                |
+----------+----------------------------------+
1 Row(s) produced. Time Elapsed: 0.620s
*/

-- More fun query, some of the longer sequences

SELECT
    count(*),
    sequence
FROM (SELECT a.seq,
             ARRAY_AGG(a.name)
                       WITHIN GROUP (ORDER BY a.id ASC) AS sequence
      FROM seqs a
               JOIN
               (SELECT DISTINCT seq AS selected_seq FROM seqs WHERE attr_0 = 'foo')
               ON seq = selected_seq
      GROUP BY a.seq)
GROUP BY
    sequence
ORDER BY
    count(*) DESC
LIMIT 10;


/*
+----------+----------+
| COUNT(*) | SEQUENCE |
|----------+----------|
|        1 | [        |
|          |   "F",   |
|          |   "D",   |
|          |   "A",   |
    ...
|          |   "F"    |
|          | ]        |
+----------+----------+
10 Row(s) produced. Time Elapsed: 23.733s
*/


-- Lets see if we can accelerate it significantly by precalculating

create or replace table seqs_precalculated (
    seq int,
    min_ts int,
    max_ts int,
    names varchar,
    attrs_0 varchar,
    attrs_1 varchar,
    attrs_2 varchar,
    attrs_3 varchar,
    attrs_4 varchar,
    attrs_5 varchar,
    attrs_6 varchar,
    attrs_7 varchar,
    attrs_8 varchar,
    attrs_9 varchar,
    PRIMARY KEY (seq)
);

INSERT INTO seqs_precalculated (seq, min_ts, max_ts, names,
                                attrs_0, attrs_1, attrs_2, attrs_3, attrs_4,
                                attrs_5, attrs_6, attrs_7, attrs_8, attrs_9)
    (SELECT seq,
            MIN(ts),
            MAX(ts),
            ARRAY_TO_STRING(ARRAY_AGG(name) WITHIN GROUP (ORDER BY id ASC), ', ')   AS names,
            ARRAY_TO_STRING(ARRAY_AGG(attr_0) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_0,
            ARRAY_TO_STRING(ARRAY_AGG(attr_1) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_1,
            ARRAY_TO_STRING(ARRAY_AGG(attr_2) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_2,
            ARRAY_TO_STRING(ARRAY_AGG(attr_3) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_3,
            ARRAY_TO_STRING(ARRAY_AGG(attr_4) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_4,
            ARRAY_TO_STRING(ARRAY_AGG(attr_5) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_5,
            ARRAY_TO_STRING(ARRAY_AGG(attr_6) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_6,
            ARRAY_TO_STRING(ARRAY_AGG(attr_7) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_7,
            ARRAY_TO_STRING(ARRAY_AGG(attr_8) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_8,
            ARRAY_TO_STRING(ARRAY_AGG(attr_9) WITHIN GROUP (ORDER BY id ASC), ', ') AS attrs_9
     FROM seqs
     GROUP BY seq);


/*
+-------------------------+
| number of rows inserted |
|-------------------------|
|                 2000000 |
+-------------------------+
2000000 Row(s) produced. Time Elapsed: 276.672*/

-- Do some selects

SELECT
    count(*),
    names
FROM seqs_precalculated
GROUP BY
    names
ORDER BY
    count(*) DESC
LIMIT 10;

/*
|        1 | F, A, E, G, B, A, B, B, C, B, E, H, A, B, B, C, A, E, A, D, H, A, B, A, E, B, A, C, B, B, A, A, A, B, F, E, A, A, H, A, C, H, A, A, B, A                                  |
+----------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
10 Row(s) produced. Time Elapsed: 0.680s
*/

-- Do select with regexes. Here we want sequences that start with five "A" events and where 4th of the events has label "foo"

SELECT
    count(*),
    names
FROM seqs_precalculated
WHERE
    regexp_like(names, '^A, A, A, A, A.*')
AND
    regexp_like(attrs_4, '^[^,]*,[^,]*,[^,]*, foo.*')
GROUP BY
    names
ORDER BY
    count(*) DESC
LIMIT 10;

/*
10 Row(s) produced. Time Elapsed: 0.427s
*/
