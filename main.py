import logging
import sys
import pandas as pd
import numpy as np

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(asctime)s %(message)s')


def generate(count: int, series: int):
    seq_start_id = 10000000
    sequences_count = count/10
    for i in range(0, series):
        logging.info(f"Generating {count} items in iteration {i}")
        df = pd.DataFrame({
            'id': np.arange(count) + count*i,
            'seq': np.random.randint(seq_start_id, seq_start_id+sequences_count, count),
            'ts': np.random.randint(1652900400 - 3600 * 24, 1652900400, count),
            'name': np.random.choice(['A', 'A', 'A', 'A', 'A', 'A', 'B', 'B', 'B', 'C', 'C', 'D', 'E', 'F', 'G', 'H'], count),
            'attr_0': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
            'attr_1': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
            'attr_2': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
            'attr_3': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
            'attr_4': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
            'attr_5': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
            'attr_6': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
            'attr_7': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
            'attr_8': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
            'attr_9': np.random.choice(['foo', 'bar', 'baz', '', '', '', '', ''], count),
        })
        filename = f"gen-{count}-{i}.pandas"
        logging.info(f"Storing {filename}")
        df.to_parquet(filename)
        seq_start_id = seq_start_id + sequences_count


if __name__ == '__main__':
    generate(10000000, 1)

